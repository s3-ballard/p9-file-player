/*
  ==============================================================================

    FilePlayerGui.h
    Created: 22 Jan 2013 2:49:07pm
    Author:  tj3-mitchell

  ==============================================================================
*/

#ifndef H_FILEPLAYERGUI
#define H_FILEPLAYERGUI

#include "../JuceLibraryCode/JuceHeader.h"
#include "FilePlayer.h"

/**
 Gui for the FilePlayer class
 */
class FilePlayerGui :   public Component,
                        public Button::Listener,
                        public FilenameComponentListener,
                        public Slider::Listener,
                        public Timer
{
public:
    /**
     constructor - receives a reference to a FilePlayer object to control
     */
    //FilePlayerGui();
    FilePlayerGui(FilePlayer& filePlayer_);
    
    /**
     Destructor 
     */
    ~FilePlayerGui();
    
    //void setFilePlayer(FilePlayer& fp) {filePlayer = &fp;}
    
    //Component
    void resized();
    
    //Button Listener
    void buttonClicked (Button* button);
    
    void sliderValueChanged (Slider* slider);
    
    void timerCallback();
    
    //FilenameComponentListener
    void filenameComponentChanged (FilenameComponent* fileComponentThatHasChanged);

private:
    TextButton playButton;
    FilenameComponent* fileChooser;
    Slider timeSlider;
    Slider pitchSlider;
    
    FilePlayer& filePlayer;

};


#endif  // H_FILEPLAYERGUI
