/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent (Audio& audio_) : audio (audio_)
{

    for (int i = 0; i < NumFilePlayers; i++)
    {
        filePlayerGui[i] = new FilePlayerGui(audio_.getFilePlayer(i));
    }
    for (int counter = 0; counter < NumFilePlayers; counter++)
    {

         addAndMakeVisible(filePlayerGui[counter]);
        
    }
    
    addAndMakeVisible(gain);
    gain.addListener(this);
    setSize (500, 900);
    gain.setRange(0, 1);
   
}

MainComponent::~MainComponent()
{

}

void MainComponent::resized()
{
    int height = 0;
    
    for (int counter = 0; counter < NumFilePlayers; counter++)
    {
    filePlayerGui[counter]->setBounds (0, height, getWidth(), 300);
        height += 300;
    }
    
    gain.setBounds(0, 650, getWidth(), 50);
}

//MenuBarCallbacks==============================================================
StringArray MainComponent::getMenuBarNames()
{
    const char* const names[] = { "File", 0 };
    return StringArray (names);
}

PopupMenu MainComponent::getMenuForIndex (int topLevelMenuIndex, const String& menuName)
{
    PopupMenu menu;
    if (topLevelMenuIndex == 0)
        menu.addItem(AudioPrefs, "Audio Prefrences", true, false);
    return menu;
}

void MainComponent::menuItemSelected (int menuItemID, int topLevelMenuIndex)
{
    if (topLevelMenuIndex == FileMenu)
    {
        if (menuItemID == AudioPrefs)
        {
            AudioDeviceSelectorComponent audioSettingsComp (audio.getAudioDeviceManager(),
                                                            0, 2, 2, 2, true, true, true, false);
            audioSettingsComp.setSize (450, 350);
            DialogWindow::showModalDialog ("Audio Settings",
                                           &audioSettingsComp, this, Colours::lightgrey, true);
        }
    }
}

void MainComponent::sliderValueChanged(Slider* slider)
{
    if (slider == &gain)
    {
        audio.gain = gain.getValue();
    }
}

