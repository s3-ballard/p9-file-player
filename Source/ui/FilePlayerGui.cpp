/*
  ==============================================================================

    FilePlayerGui.cpp
    Created: 22 Jan 2013 2:49:07pm
    Author:  tj3-mitchell

  ==============================================================================
*/

#include "FilePlayerGui.h"

FilePlayerGui::FilePlayerGui (FilePlayer& filePlayer_) : filePlayer (filePlayer_)
{
    playButton.setButtonText (">");
    playButton.addListener(this);
    addAndMakeVisible(&playButton);
    
    timeSlider.addListener(this);
    addAndMakeVisible(&timeSlider);
    timeSlider.setRange(0.0, 1.0);
    
    pitchSlider.setRange(0.01, 5.0);
    addAndMakeVisible(&pitchSlider);
    pitchSlider.addListener(this);
    pitchSlider.setValue(1.0);
    
    AudioFormatManager formatManager;
    formatManager.registerBasicFormats();
    fileChooser = new FilenameComponent ("audiofile",
                                         File::nonexistent,
                                         true, false, false,
                                         formatManager.getWildcardForAllFormats(),
                                         String::empty,
                                         "(choose a WAV or AIFF file)");
    fileChooser->addListener(this);
    addAndMakeVisible(fileChooser);
    
}

FilePlayerGui::~FilePlayerGui()
{
    delete fileChooser;
}


//Component
void FilePlayerGui::resized()
{
    playButton.setBounds (0, 0, getHeight(), 50);
    fileChooser->setBounds (getHeight(), 0, getWidth()-getHeight(), 50);
    timeSlider.setBounds(0, 50, getWidth(), 100);
    pitchSlider.setBounds(0, 150, getWidth(), 100);
    
}

//Button Listener
void FilePlayerGui::buttonClicked (Button* button)
{
    if (button == &playButton)
    {
        filePlayer.setPlaying(!filePlayer.isPlaying());
        if (filePlayer.isPlaying() == true)
        {
            startTimer(250);
        }
        
        else if (filePlayer.isPlaying() == false)
        {
            stopTimer();
        }
        
    }
}

//FilenameComponentListener
void FilePlayerGui::filenameComponentChanged (FilenameComponent* fileComponentThatHasChanged)
{
    if (fileComponentThatHasChanged == fileChooser)
    {
        File audioFile (fileChooser->getCurrentFile().getFullPathName());
        
        if(audioFile.existsAsFile())
        {
                filePlayer.loadFile(audioFile);
        }
        else
        {
            AlertWindow::showMessageBox (AlertWindow::WarningIcon,
                                         "sdaTransport",
                                         "Couldn't open file!\n\n");
        }
    }
}

void FilePlayerGui::sliderValueChanged(Slider* slider)
{
    if (slider == &timeSlider)
    {
        DBG ("slider moved");
        filePlayer.setPosition(timeSlider.getValue());
    }
    
    if (slider == &pitchSlider)
    {
        filePlayer.setPlaybackRate(pitchSlider.getValue());
    }
    
    
}

void FilePlayerGui::timerCallback()
{
    timeSlider.setValue(filePlayer.getPosition());
}
